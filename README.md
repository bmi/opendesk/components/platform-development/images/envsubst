# envsubst

A very simple docker file to create an [Alpine Linux](https://alpinelinux.org/) based container image with [gettext](https://www.gnu.org/software/gettext/) installed.

It comes handy when you want to replace placeholders within text documents with values from environment variables.

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
