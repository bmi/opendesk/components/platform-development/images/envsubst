# 1.0.0 (2023-12-27)


### Bug Fixes

* **docs:** Add links ([38ffc2b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/envsubst/commit/38ffc2b6af640bfc57dd8ec884a7ddb8b36d7971))


### Features

* Initial commit ([77e636d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/envsubst/commit/77e636d8fdad3b55a72d5b08bca869722baeb670))
